function getCode() {
    return window.idpCode + "\n"
        + "theory T : V {\n"
        + window.mirror.getValue()
        + "}"
}

function visualiseMap(colorMap) {
    if (typeof lay != "undefined") {
        lay.remove();
    }

    function getColor(d) {
        d = d.toLowerCase();
        return colorMap[d];
    }

    function style(feature) {
        return {
            fillColor: getColor(feature.properties.name),
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7
        };
    }

    window.lay = L.geoJson(euCountries, {style: style}).addTo(map);

}

function run() {
    var r = {
        "code": getCode(),
        "GV": 0,
        "SV": 0,
        "NB": 0,
        "inference": 2,
        "stable": false,
        "cp": false,
        "filename": "main",
        "eInput": ""
    };
    $.ajax({
        type: 'POST',
        url: "https://adams.cs.kuleuven.be/idp/eval",
        data: JSON.stringify(r),
        async: false
    }).done(function (result) {
        if (result.stdout != "") {
            $("#map").show()
            $("#errorPane").hide()
            var vis = JSON.parse(result.stdout);
            visualiseMap(vis)
        } else {
            $("#map").hide()
            $("#errorPane").show()
            $("#errorPane").text(result.stderr)
        }

    })

}

function load(i) {
    var scenario = $.ajax({
        url: "./s" + i + ".idp",
        async: false
    }).responseText;
    window.mirror.setValue(scenario);
}
