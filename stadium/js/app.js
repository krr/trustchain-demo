function Rule(explanation, constraint, isActive) {
    this.explanation = explanation;
    this.constraint = constraint;
    this.isActive = isActive;
}

function EntitlementProperty(explanation, idpName, isPresent) {
    this.explanation = explanation;
    this.idpName = idpName;
    this.isPresent = isPresent;
}

angular.module('TrustChainApp', ['ngAnimate', 'angular-loading-bar'])
    .directive('tooltip', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                $(element).hover(function () {
                    // on mouseenter
                    $(element).tooltip('show');
                }, function () {
                    // on mouseleave
                    $(element).tooltip('hide');
                });
            }
        };
    })
    .controller('TrustChainController', ['$scope', '$http', '$animate', function ($scope, $http) {
        var trustchain = this;
        this.entitlement = [];

        this.rules = [];

        this.entitleReset = function () {
            trustchain.entitlement.forEach(function (i) {
                i.isPresent = false
            });
            trustchain.entitlement[0].isPresent = true;
            trustchain.entitlement[1].isPresent = true;
            trustchain.entitlement[6].isPresent = true;
            trustchain.doCheck()

        };

        this.ruleReset = function () {
            trustchain.rules.forEach(function (i) {
                i.isActive = true
            });
            trustchain.rules[0].isActive = false;
            trustchain.doCheck()
        };

        this.entryAllowed = false;
        this.violations = [];

        this.constructIDPFile = function () {
            var allProps = [];
            var activeProps = [];
            var nonactiveProps = [];
            trustchain.entitlement.forEach(function (t) {
                allProps.push(t.idpName);
                if (t.isPresent) {
                    activeProps.push(t.idpName);
                } else {
                    nonactiveProps.push(t.idpName);
                }
            });


            var file = [
                "vocabulary V {",
                "type axioms = {0.."+(trustchain.rules.length-1)+"} isa int",
                "fails(axioms)",
                "",
                allProps.join('\n'),
                "} ",
                "",
                "theory T : V {",
                "{",
                trustchain.rules.map(function (t,i) {
                    return "fails("+i+") <- ~("+t.constraint + ")."
                }).join('\n'),
                "}",
                "}",
                "",
                "structure S : V {",
                activeProps
                    .map(function (t) {
                        return t + ' = true'
                    })
                    .join('\n'),
                nonactiveProps
                    .map(function (t) {
                        return t + ' = false'
                    })
                    .join('\n'),
                "}",
                "",
                "include <table_utils>",
                "procedure main(){",
                "result = calculatedefinitions(T,S)[V::fails].ct",
                "json = require \"idpd3/dkjson\"",
                "print(json.encode(totable(result)))",
                "}"
            ].join('\n');
            console.log(file)
            return file
        };

        this.doCheck = function () {
            var r = {"code" : trustchain.constructIDPFile() ,
                     "inference" : 2
                    };
            $http({
                method: 'POST',
                url: "http://adams.cs.kuleuven.be/idp/eval",
                data: JSON.stringify(r)
            }).then(function(succes){
                var result = succes.data;
                if(result.stdout != ""){
                    console.log(result.stdout);
                    var fails = JSON.parse(result.stdout);
                    var entrystatus = true
                    trustchain.violations = [];

                    fails.map(function (t) {
                        var failedRule = trustchain.rules[t[0]];
                        if(failedRule.isActive){
                            entrystatus = false;
                            trustchain.violations.push(failedRule.explanation)
                        }
                    });
                    trustchain.entryAllowed = entrystatus;
                }else{
                    console.log(result.stderr)
                }
            })
        };




        //This gets the data from the json file and initialises the actual config process
        $http.get('js/trustchain.json').then(function (data) {
            var json = data.data;
            trustchain.entitlement = json.properties;
            trustchain.rules = json.rules;
            trustchain.doCheck();
        });

    }]);

