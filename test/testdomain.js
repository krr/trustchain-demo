// sample on how to create an entitlement evaluator

// format must be Validate_<DomainName>
function Validate_testdomain( Entitlement ) {
    // returned result is JSON object
    var result={};

    // if boolean return,
    result.result = "All ok from Validate_testdomain";
    
    return result;
}
