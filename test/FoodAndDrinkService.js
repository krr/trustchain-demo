// this function returns the available functions in this service
function getMethods() {

    return {
        'getOrderList' : {
        },
        'startOrder' : {
            'orderid' : '',
            'by' : ''
        },
        'endOrder' : {
            'orderid' : '',
            'by' : '',
            'status' : true
        },
        'createOrder' : {
            'for' : '',
            'products' : []
        },
        'cancelOrder' : {
            'orderid' : '',
            'for' : ''
        },
        'getProductList' : {
        },
        'getNextWaitingOrderCount' : {
        },
        'getReadyOrderCount' : {
        },
        'startNextWaitingOrder' : {
        },
        'preparedOrder' : {
            'orderid' : ''
        },
        'reset' : {
        }
    };
}

function initialize() {
    var Ctx = Context.get();

    if( Ctx.pendingorders === undefined ) {
        Ctx.pendingorders = {};
        Ctx.orderforwhom = {};
        Ctx.ordersequence = [];
        Context.set( Ctx );
    }

    return Ctx;

}

function getOrderList( ArgList ) {
    var All = ArgList[ 'all' ];
    var Ctx = initialize();

    var Orders = [];
    for( var i in Ctx.ordersequence ) {
        var key = Ctx.ordersequence[i];
        if( Ctx.pendingorders[key].state === 'ready' ) {
            Orders.push( {
                            'key' : key,
                            'forwhom' : Ctx.pendingorders[key]["forwhom"],
                            'orders' : Ctx.pendingorders[key]["products"],
                            'meridian' : Ctx.pendingorders[key]['meridian']
                          }  );
            if( ! All ) {
                break;
            }
        }
    }

    return { 'orders' : Orders };
}

function startOrder( ArgList ) {
    var Order = ArgList['orderid'];
    var By = ArgList[ 'by' ];
    var Ctx = initialize();

    var TheOrder = Ctx.pendingorders[ Order ];

    if( TheOrder === undefined ) {
        return { 'error' : 'Order ' + Order + ' no longer exists' };
    }

    if( TheOrder.state === undefined ) {
        return { 'error' : 'Order ' + Order + ' in wait queue' }
    }

    if( TheOrder.state === 'preparing' ) {
        return { 'error' : 'Order ' + Order + ' is being prepared' }
    }

    if( TheOrder.state !== 'ready' ) {
        return { 'error' : 'Order ' + Order + ' being handled by ' + TheOrder.state };
    }

    Ctx.pendingorders[ Order ].state = By;
    Context.set( Ctx );

    return { 'result' : 'Order ' + Order + ' started' };
}

function endOrder( ArgList ) {
    var Order = ArgList[ 'orderid' ];
    var By = ArgList[ 'by' ];
    var Status = ArgList[ 'status' ];
    var Ctx = initialize();

    var TheOrder = Ctx.pendingorders[ Order ];

    if( TheOrder === undefined ) {
        return { 'error' : 'Order ' + Order + ' no longer exists' };
    }

    if( TheOrder.state !== By ) {
        return { 'error' : 'Order ' + Order + ' not handled by ' + By };
    }

    delete Ctx.orderforwhom[ Ctx.pendingorders[ Order ].forwhom ];
    delete Ctx.pendingorders[ Order ];

    var idx = Ctx.ordersequence.indexOf( Order );
    if( idx >= 0 ) {
        Ctx.ordersequence.splice( idx, 1 );
    }

    Context.set( Ctx );

    return { 'orders' : 'Order ' + Order + ' finished' };
}

function createOrder( ArgList ) {
    var For = ArgList[ 'for' ];
    var Products = ArgList[ 'products' ];
    var Ctx = initialize();

    if( Products.length === 0 ) {
        return { 'error' : 'Empty order not allowed' };
    }

    if( Ctx.orderforwhom[ For ] !== undefined ) {
        return { 'error' : 'Order pending, only one order allowed' };
    }

    // add order
    var Order = TCD.generateUUID();

    var Result = TCD.send( "findmyfriends",
                  "getUserProperties",
                  { "user" : For }
                  );

    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    Ctx.pendingorders[ Order ] = {
        'forwhom' : For,
        'products' : Products,
        'meridian' : Result.result
    };

    Ctx.orderforwhom[ For ] = Order;
    Ctx.ordersequence.push( Order );

    Context.set( Ctx );

    return { 'orderid' : Order };
}

function startNextWaitingOrder( ArgList ) {
    var Ctx = initialize();

    var Order = {};
    var OrderID;

    for( var i in Ctx.ordersequence ) {
        var key = Ctx.ordersequence[i];
        if( Ctx.pendingorders[key].state === undefined ) {
            Order = Ctx.pendingorders[ key ];
            Ctx.pendingorders[key].state = 'preparing';
//            Context.set(Ctx);
            OrderID = key;
            break;
        }
    }

    return { 'orderid' : OrderID, 'order' : Order };
}

function preparedOrder( ArgList ) {
    var Order = ArgList[ 'orderid' ];
    var Ctx = initialize();

    var idx = Ctx.ordersequence.indexOf( Order );
    console.log( typeof Ctx.ordersequence[0] );
    console.log( typeof Order);
    if( idx >= 0 ) {
        Ctx.pendingorders[ Order ].state = 'ready';
        Context.set( Ctx );

        return { 'result' : 'Order ready for delivery' };
    }

    return { 'error' : 'Order ' + Order + ' not known' };

}

function cancelOrder( ArgList ) {
    var For = ArgList[ 'for' ];
    var Order = ArgList[ 'orderid' ];
    var Ctx = initialize();

    if( Ctx.orderforwhom[ For ] === undefined ) {
        return { 'error' : 'No order known for ' + For };
    }

    if( Ctx.orderforwhom[ For ] != Order ) {
        return { 'error' : Order + ' is not the current order for ' + For };
    }

    if( Ctx.pendingorders[ Order ].state === 'preparing' ) {
        return { 'error' : Order + ' is being prepared' };
    }

    if( Ctx.pendingorders[ Order ].state !== 'ready' ) {
        return { 'error' : Order + ' is ready for delivery' };
    }

    if( Ctx.pendingorders[ Order ].state !== undefined ) {
        return { 'error' : 'Order ' + Order + ' is being delivered' }
    }

    delete Ctx.pendingorders[ Order ];
    delete Ctx.orderforwhom[ For ];

    var idx = Ctx.ordersequence.indexOf( Order );
    if( idx >= 0 ) {
        Ctx.ordersequence.splice( idx, 1 );
    }

    Context.set( Ctx );

    return { 'result' : 'Order ' + Order + ' cancelled' };

}

function getProductList( ArgList ) {
    var Result;

    return { 'products' : DataContext.products };
}

function getNextWaitingOrderCount() {
    var Ctx = initialize();
    var Ct = 0;

    for( var i in Ctx.ordersequence ) {
        var key = Ctx.ordersequence[i];
        if( Ctx.pendingorders[key].state === undefined ) {
            Ct ++;
        }
    }

    return { "result" : Ct };

}

function getReadyOrderCount() {
    var Ctx = initialize();
    var Ct = 0;

    for( var i in Ctx.ordersequence ) {
        var key = Ctx.ordersequence[i];
        if( Ctx.pendingorders[key].state === 'ready' ) {
            Ct ++;
        }
    }

    return { "result" : Ct };
}

function reset( ArgList ) {
    var Ctx = Context.get();

    delete Ctx.pendingorders;
    delete Ctx.orderforwhom;
    delete Ctx.ordersequence;

    Context.set( Ctx );
    return { 'result' : 'Reset' };
}
