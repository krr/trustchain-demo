// sample on how to create an entitlement evaluator

// this function returns the available functions in this service
function getMethods() {
  
  return {
    // key is name of function, value is JSON object describing ArgList object
    'validate_Domain' : {
      'entitlement' : ''
    }
  };
}

// format must be Validate_<DomainName>
// arglist is object with entitlement and extradata property
function validate_Domain( ArgList ) {
    // returned result is JSON object
    var result={};

    // Entitlement is a JSON object

    if( SomeError ) {
        // if Some error occured return JSON with 'error' property
        result.error = "Some error message";
        return result;
    }

    // if boolean return,
    result.result = true;

    // if more complex return
    result.result.complex1 = 'foo';
    result.result.complext2 = { 'bar' : 1, 'baz' : 2 };

    return result;
}
