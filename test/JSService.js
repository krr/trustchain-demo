// this function returns the available functions in this service
function getMethods() {

    return {
        // key is name of function, value is JSON object describing ArgList object
        'testMethod' : {
            'arg' : ''
        }
    };
}

// arglist is object
function testMethod( ArgList ) {
    var Result = { 'result' : 'OK' };
    console.log( json.stringify( ArgList ) );

    return Result;
}
