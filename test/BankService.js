// this function returns the available functions in this service
function getMethods() {

    return {
        'addAccount' : {
            'storeid' : '',
            'accountid' : ''
        },
        'removeAccount' : {
            'storeid' : '',
            'accountid' : ''
        },
        'getAccountList' : {
            'storeid' : ''
        },
        'requestCash' : {
            'accountentitlement' : '',
            'amount' : 0
        },
        'revokeQuickCashAccess' : {
            'tostoreid' : '',
            'forstoreid' : ''
        },
        'requestQuickCashAccess' : {
            'tostoreid' : '',
            'foraccountentitlement' : '',
            'forstoreid' : ''
        },
        'getQuickCashAccessList' : {
            'tostoreid' : ''
        },
        'setupQuickCash' : {
            'foraccountaccessentitlement' : '',
            'oldsoeid' : '',
            'amount' : 0
        },
        'reset' : {
            'storeid': ''
        },
        'validate_account' : {
            'entitlement' : '',
            'extradata' : {}
        },
        'validate_cashrequestso' : {
            'entitlement' : '',
            'extradata' : {}
        },
        'validate_cashrequest' : {
            'entitlement' : '',
            'amount' : 0,
            'extradata' : {}
        }
    };
}

function cashRequestEntitlementName( StoreID, AccountID ) {
    return StoreID + '-' + AccountID + "-CashRequest";
}

function reset( ArgList ) {
    Result = TCD.send( "storemanager",
                      "resetStore",
                      { "storeid" :  ArgList['storeid'] }
                      );


    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    return { "result" : "All reset" };
}

function addAccount( ArgList ) {
    var Result;
    var StoreID = ArgList[ 'storeid' ];
    var AcId = ArgList['accountid'];

    var AccountEnt = {
        'maindomain' : 'account',
        'entitlementid' : TCD.generateUUID(),
        'name' : "Account " + AcId,
        'domains' : [ 'account' ],
        'issuedtoid' : StoreID,
        'issuerid' : 'bank',
        'publicproperties' : {
            'id' : AcId
        }
    };

    Result = TCD.send( "storemanager",
                      "addEntitlement",
                      { "entitlement" : AccountEnt }
                      );

    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    return { "result" : "Account " + AcId + "-" + StoreID + " created" };
}

function removeAccount( ArgList ) {
    var Result;
    var StoreID = ArgList[ 'storeid' ];
    var AcId = ArgList['accountid'];

    Result = TCD.send( "storemanager",
                      "getEntitlementByDomain",
                      { "issuedtoid" :  StoreID,
                        "domain" : 'account'
                      }
                      );

    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    var RawEnts = [];
    var Ents = Result.result.entitlements;

    for( var E in Ents ) {
        if( Ents[E].publicproperties.id === AcId ) {
            Result = TCD.send( "storemanager",
                              "removeEntitlement",
                              { 'issuedtoid' : StoreID,
                                'entitlementid' : Ents[E].entitlementid
                              }
                              );

            if( Result.error !== undefined ) {
                // do NOT commit the context
                return Result;
            }

            return { "result" : "Account " + AcId + "-" + StoreID + " removed" };
        }
    }

    return { "result" : "Account " + AcId + "-" + StoreID + " not found" };
}

// arglist is object
function getAccountList( ArgList ) {
    var Result;

    // request ticket from storemamanger
    Result = TCD.send( "storemanager",
                      "getEntitlementByDomain",
                      { "issuedtoid" :  ArgList['storeid'],
                        "domain" : 'account'
                      }
                      );

    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    var RawEnts = [];
    var Ents = Result.result.entitlements;

    for( var E in Ents ) {
        RawEnts.push( Ents[E] );
    }

    return { "accounts" : RawEnts };
}

function requestCash( ArgList ) {
    var Result;
    var AEnt = ArgList[ 'accountentitlement' ];
    var StoreID = AEnt.issuedtoid;
    var Amount = ArgList[ 'amount' ];

    // transfer the ticket for a particular game
    var CashReqEnt = {
        'maindomain' : 'cashrequest',
        'entitlementid' : cashRequestEntitlementName(StoreID, AEnt.publicproperties.id ),
        'name' : "Cash from account " + AEnt.name,
        'domains' : [ 'cashrequest' ],
        'issuedtoid' : StoreID,
        'issuerid' : 'bank',
        'publicproperties' : {
            'amount' : Amount,
            'type' : 'singleuse',
            'accountentitlement' : AEnt
        }
    };

    Result = TCD.send( "storemanager",
                      "addEntitlement",
                      { "entitlement" : CashReqEnt,
                        'allowupdate' : true }
                      );

    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    return { "result" : "Cash request created" };

}

function hasQuickCashAccessFor( ToStoreID, ForStoreID ) {
    var Result = TCD.send( "storemanager",
                      "getEntitlementByDomain",
                      { "issuedtoid" :  ToStoreID,
                        "domain" : 'quickcashaccess'
                      }
                      );

    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    var Ents = Result.result.entitlements;

    for( var E in Ents ) {
        if( Ents[E].publicproperties.accountfrom === ForStoreID ) {
            return Ents[E];
        }
    }

    return null;
}

/*
     Account -> gives QuickCash access to somebody -> creates SO

     can only be one QuickCash access per account
*/

// removes right for QA of tostoreid for storeid
function revokeQuickCashAccess( ArgList ) {
    var Result;
    var ToStoreID = ArgList[ 'tostoreid' ];
    var ForStoreID = ArgList[ 'forstoreid' ];

    // already assigned QcA ?
    var AssignedEnt = hasQuickCashAccessFor( ToStoreID, ForStoreID );

    if( AssignedEnt !== null ) {
        // revoke
        var Args = { "issuedtoid" : ToStoreID,
                     "entitlementid" : AssignedEnt.entitlementid
            };

        // request ticket from storemamanger
        Result = TCD.send( "storemanager", "removeEntitlement", Args );

        if( Result.error !== undefined ) {
            return Result;
        }

        return { "result" : ToStoreID + " QuickCash access for " + ForStoreID + " revoked" };
    }

    return { "error" : ToStoreID + " has no QuickCash access for " + ForStoreID };
}

/*
  gives Tostoreid QuickCash access to ForAcE of ForStoreid
  only one allowed
*/
function requestQuickCashAccess( ArgList ) {
    var ToStoreID = ArgList[ 'tostoreid' ];
    var ForAcE = ArgList[ 'foraccountentitlement' ];
    var ForStoreID = ForAcE.issuedtoid;

    var AssignedQCA = hasQuickCashAccessFor( ToStoreID, ForStoreID );

    if( AssignedQCA !== null ) {
        return { "result" : ToStoreID + " alread has QuickCash access for " + ForStoreID };
    }

    // OK not yet assigned
    var QuickCashAccess = {
        'maindomain' : 'quickcashaccess',
        'entitlementid' : TCD.generateUUID(),
        'name' : "QCA " + ForAcE.issuedtoid +
                 "-" + ForAcE.publicproperties.id,
        'domains' : [ 'quickcashaccess' ],
        'issuedtoid' : ToStoreID,
        'issuerid' : 'bank',
        'publicproperties' : {
            'accountfrom' : ForStoreID,
            'accountentitlement' : ForAcE
        }
    };

    var Result = TCD.send( "storemanager",
                      "addEntitlement",
                      { "entitlement" : QuickCashAccess }
                      );

    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    return { "result" : "Quickcash access for " + ForStoreID + " granted to " + ToStoreID };
}

// get list of assigned QCA assigned to ToStoreID
function getQuickCashAccessList( ArgList ) {
    var ToStoreID = ArgList[ 'tostoreid' ];
    var Result;

    // request QcA's from storemamanger
    Result = TCD.send( "storemanager",
                      "getEntitlementByDomain",
                      { "issuedtoid" : ToStoreID,
                        "domain" : 'quickcashaccess'
                      }
                      );

    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    var RawEnts = [];
    var AxsEnts = Result.result.entitlements;

    // request the SO (if they exist) for each of the AxsEnts
    var AllEnrichedAxsEnts = [];
    for( var AEID in AxsEnts ) {
        var Enriched;

        var SOResult1 = TCD.send( "storemanager",
                          "getEntitlementByDomain",
                          { "issuedtoid" :  AxsEnts[AEID].publicproperties.accountentitlement.issuedtoid,
                            "domain" : 'cashrequestso'
                          }
                          );

        Enriched = { 'accessentitlement' : AxsEnts[AEID],
                     'quickcashsoeid' : '',
                     'quickcashsoamount' : -1 };

        // find in that list if there is an SO associated with our QcA
        if( SOResult1.error === undefined ) {
            for( var SOE in SOResult1.result.entitlements ) {
                var SOEnt = SOResult1.result.entitlements[ SOE ];
                if( SOEnt.publicproperties.setupby === ToStoreID ) {
                    // this SO is setup by ToStoreID
                    Enriched.quickcashsoeid = SOEnt.entitlementid;
                    Enriched.quickcashsoamount = SOEnt.publicproperties.amount;
                    break;
                }
            }
        }

        AllEnrichedAxsEnts.push( Enriched );

    }

    return { "quickcashso" : AllEnrichedAxsEnts };
}

function setupQuickCash( ArgList ) {
    var Result;
    var ForAcE = ArgList[ 'foraccountaccessentitlement' ];
    var PreviousSOEid = ArgList[ 'oldsoeid' ];
    var Amount = ArgList[ 'amount' ];

    // should validate entitlement
//    var VResult = TCD.send( "storemanager",
//                      "validateEntitlement",
//                      { "entitlement" :  ForAcE
//                      }
//                      );
//    if( VResult.error !== undefined ) {
//       return VResult;
//    }

    if( Amount === 0 ) {
        if( PreviousSOEid !== '' ) {
            // revoke
            var Args = { "issuedtoid" : ForAcE.publicproperties.accountfrom,
                         "entitlementid" : PreviousSOEid
                };

            // request ticket from storemamanger
            Result = TCD.send( "storemanager", "removeEntitlement", Args );

            if( Result.error !== undefined ) {
                return Result;
            }

            return { "result" : "QuickCash order revoked for " +  ForAcE.publicproperties.accountfrom };
        }

        // ignore
        return { "error" : "Cannot update to amount 0" };
    }

    // create (or update if amount >= 0) Standing order
    var CashReqEnt = {
        'maindomain' : 'cashrequest',
        'name' : "SOCash from account " + ForAcE.publicproperties.accountentitlement.name,
        'domains' : [ 'cashrequest', 'cashrequestso' ],
        'issuedtoid' : ForAcE.publicproperties.accountfrom,
        'issuerid' : 'bank',
        'publicproperties' : {
            'amount' : Amount,
            'type' : 'multiuse',
            'setupby' : ForAcE.issuedtoid,
            'accountaccessentitlement' : ForAcE
        }
    };

    CashReqEnt.entitlementid = cashRequestEntitlementName(
                ForAcE.publicproperties.accountfrom,
                ForAcE.publicproperties.accountentitlement.publicproperties.id );
    Result = TCD.send( "storemanager",
                  "addEntitlement",
                  { "entitlement" : CashReqEnt,
                    'allowupdate' : true }
                  );

    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    return { "result" : "SO Quick Cash set up" };

}

function validate_account( ArgList ) {
    var Result = { "result" : "valid account" };
    return Result;
}

function validate_cashrequestso( ArgList ) {
    var Result = { "result" : "valid so request" };
    return Result;
}

function validate_cashrequest( ArgList ) {
    var Result = { "result" : "valid request" };
    return Result;
}

