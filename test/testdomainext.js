// this function returns the available functions in this service
function getMethods() {
  
  return {
    // key is name of function, value is JSON object describing ArgList object
    'validate_testdomainext' : {
      'entitlement' : ''
    }
  };
}

function validate_testdomainext( Entitlement ) {
    // returned result is JSON object
    var result={};

    // if boolean return,
    result.result = "All ok from Validate_testdomainext";
    
    return result;
}
