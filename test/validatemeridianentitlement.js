// sample on how to create an entitlement evaluator

// this function returns the available functions in this service
function getMethods() {
  
  return {
    // key is name of function, value is JSON object describing ArgList object
    'validate_meridian' : {
      'entitlement' : '',
      'extradata' : {}
    }
  };
}

// format must be Validate_<DomainName>
function validate_meridian( ArgList ) {
    var Entitlement = ArgList['entitlement'];
    // returned result is JSON object
    var result={};
	var SomeError = false;

    // Entitlement is a JSON object

    if( Entitlement.publicproperties.Duration !== undefined ) {
        console.log( Entitlement.publicproperties.Duration );
        result.result = "valid";
        return result;
    }

    if( SomeError ) {
        // if Some error occured return JSON with 'error' property
        result.error = "Some error message";
        return result;
    }

    // if boolean return,
    result.result = true;

    // if more complex return
    result.result.complex1 = 'foo';
    result.result.complext2 = { 'bar' : 1, 'baz' : 2 };

    return result;
}
