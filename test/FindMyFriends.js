// this function returns the available functions in this service
function getMethods() {

    return {
        'createGroup' : {
            'groupname' : ''
        },
        'listGroups' : {
        },
        'deleteGroup' : {
            'groupname' : ''
        },
        'listGroupMembers' : {
            'groupname' : ''
        },
        'addToGroup' : {
            'user' : '',
            'groupname' : ''
        },
        'removeFromGroup' : {
            'user' : '',
            'groupname' : ''
        },
        'setMeridianKey' : {
            'user' : '',
            'key' : ''
        },
        'setMeridianPassword' : {
            'user' : '',
            'password' : ''
        },
        'setNickname' : {
            'user' : '',
            'nickname' : ''
        },
        'setInviteKey' : {
            'user' : '',
            'key' : ''
        },
        'getUserProperties' : {
            'user' : ''
        },
        'reset' : {
        }
    };
}

function initialize() {
    var Ctx = Context.get();

    if( Ctx.grouplist === undefined ) {
        Ctx.grouplist = {};
        Ctx.userlist = {};
        Context.set( Ctx );
    }

    return Ctx;

}

function createuser( User ) {
    return {
        'storeid' : User,
        'meridiankey' : '',
        'invitekey' : '',
        'meridianpassword' : '',
        'nickname' : ''
    };
}

// arglist is object
function createGroup( ArgList ) {
    var Result;
    var groupname = ArgList['groupname'];
    var Ctx = initialize();

    if( Ctx.grouplist[ groupname ] !== undefined ) {
        // exists
        Result = { 'error' : "Group " + groupname + " exists" };
        Context.set( Ctx );
        return Result;
    }

    Ctx.grouplist[ groupname ] = []

    Context.set( Ctx );

    Result = { "result" : "Group " + groupname + " created" };

    return Result;
}

function listGroups( ArgList ) {
    var Result;
    var Ctx = Context.get();

    Result['groups'] = Object.keys( Ctx.grouplist );

    return Result;
}

function deleteGroup( ArgList ) {
    var GroupName = ArgList['groupname'];
    var Ctx = Context.get();

    if( Ctx.grouplist[GroupName] === undefined ) {
        return { 'error' : 'Group ' + GroupName + ' does not exist' };
    }

    delete Ctx.grouplist[ GroupName ];
    Context.set( Ctx );

    return { 'result' : 'Group ' + GroupName + ' removed' };
}

function listGroupMembers( ArgList ) {
    var GroupName = ArgList[ 'groupname' ];
    var Ctx = initialize();

    if( Ctx.grouplist[GroupName] === undefined ) {
        return { 'error' : 'Group ' + GroupName + ' does not exist' };
    }

    var Members = [];
    for( var User in Ctx.grouplist[GroupName] ) {
        var ue = Ctx.userlist[ Ctx.grouplist[GroupName][User] ];
        if( ue !== undefined ) {
            Members.push( ue );
        }
    }

    return { 'members' : Members };
}

function addToGroup( ArgList ) {
    var GroupName = ArgList[ 'groupname' ];
    var User = ArgList[ 'user' ];
    var Ctx = initialize();

    if( Ctx.grouplist[GroupName] === undefined ) {
        return { 'error' : 'Group ' + GroupName + ' does not exist' };
    }

    if( Ctx.userlist[User] === undefined ) {
        return { 'error' : 'User ' + User + ' does not exist' };
    }

    var idx = Ctx.grouplist[GroupName].indexOf( User );
    if( idx >= 0 ) {
        return { 'error' : 'User ' + User + ' already belongs to group ' + GroupName };
    }

    Ctx.grouplist[GroupName].push( User );
    Context.set( Ctx );

    return { 'result' : 'User ' + User + ' added to group ' + GroupName };
}

function removeFromGroup( ArgList ) {
    var GroupName = ArgList[ 'groupname' ];
    var User = ArgList[ 'user' ];
    var Ctx = initialize();

    if( Ctx.grouplist[GroupName] === undefined ) {
        return { 'error' : 'Group ' + GroupName + ' does not exist' };
    }

    if( Ctx.userlist[User] === undefined ) {
        return { 'error' : 'User ' + User + ' does not exist' };
    }

    var idx = Ctx.grouplist[GroupName].indexOf( User );
    if( idx < 0 ) {
        return { 'error' : 'User ' + User + ' does not belong to group ' + GroupName };
    }

    Ctx.grouplist[GroupName].splice( idx, 1 );
    Context.set( Ctx );

    return { 'result' : 'User ' + User + ' removed from group ' + GroupName };
}

function setMeridianKey( ArgList ) {
    var Key = ArgList[ 'key' ];
    var User = ArgList[ 'user' ];
    var Ctx = initialize();

    if( Ctx.userlist[User] === undefined ) {
        Ctx.userlist[User] = createuser( User );
    }

    Ctx.userlist[User].meridiankey = Key;
    Context.set( Ctx );

    return { 'result' : 'Meridian key for User ' + User + ' set' };
}

function setInviteKey( ArgList ) {
    var Key = ArgList[ 'key' ];
    var User = ArgList[ 'user' ];
    var Ctx = initialize();

    if( Ctx.userlist[User] === undefined ) {
        Ctx.userlist[User] = createuser( User );
    }

    Ctx.userlist[User].invitekey = Key;
    Context.set( Ctx );

    return { 'result' : 'Invite key for User ' + User + ' set' };

}

function setMeridianPassword( ArgList ) {
    var Password = ArgList[ 'password' ];
    var User = ArgList[ 'user' ];
    var Ctx = initialize();

    if( Ctx.userlist[User] === undefined ) {
        Ctx.userlist[User] = createuser( User );
    }

    Ctx.userlist[User].meridianpassword = Password;
    Context.set( Ctx );

    return { 'result' : 'Meridian password for User ' + User + ' set' };

}

function setNickname( ArgList ) {
    var Nickname = ArgList[ 'nickname' ];
    var User = ArgList[ 'user' ];
    var Ctx = initialize();

    if( Ctx.userlist[User] === undefined ) {
        Ctx.userlist[User] = createuser( User );
    }

    Ctx.userlist[User].nickname = Nickname;
    Context.set( Ctx );

    return { 'result' : 'Nickname for User ' + User + ' set' };
}

function getUserProperties( ArgList ) {
    var User = ArgList[ 'user' ];
    var Ctx = initialize();

    if( Ctx.userlist[User] === undefined ) {
        return { 'result' : {} };
    }

    return { 'result' : Ctx.userlist[User] };
}

function reset( ArgList ) {
    var Ctx = Context.get();

    delete Ctx.grouplist;
    delete Ctx.userlist;

    Context.set( Ctx );
    return { 'result' : 'Reset' };
}
