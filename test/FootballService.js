// this function returns the available functions in this service
function getMethods() {

    return {
        // key is name of function, value is JSON object describing ArgList object

        // generate entitlement for season ticket for give id
        'issueSeasonTicket' : {
            'issuedtoid' : ''
        },
        // generate entitlement for season ticket for give id
        'issueSeasonTicketByEntitlement' : {
            'entitlement' : ''
        },
        // transfer ticket : give a ticket for a date to another person (if NOT yet given to another person)
        'transferTicket' : {
            'issuedtoid' : '',
            'transfertoid' : '',
            'gameid' : ''
        },
        // revoke ticket to : revoke the ticket so it is available again
        'revokeTicket' : {
            'issuedtoid' : '',
            'transferredtoid' : '',
            'gameid' : ''
        },
        // revoke ticket to : revoke the ticket so it is available again
        'listTransferredTickets' : {
            'issuedtoid' : ''
        },
        // validate_ticket -> validate entitlement for domain ticket
        'validate_ticket' : {
            'entitlement' : '',
            'extradata' : {}
        },
        // set Stadion ban for : set stadion ban in context for store
        'setBanFor' : {
            'issuedtoid' : '',
            'gameid' : '',
            'banned' : true
        },
        // get game list : get list of games
        'getGameList' : {
        },
        // reset : resets context to initial state
        'reset' : {
            'storeid' : '',
        }
    };
}

function initialize() {
    var Ctx = Context.get();

    if( Ctx.bans === undefined ) {
        // by game and then the IDs which were banned
        Ctx.bans = { };
        // by userID
        Ctx.seasontickets = { };
        // by game, from to
        Ctx.transferredtickets = {};
        Ctx.accesslog = [];
        Context.set( Ctx );
    }

    return Ctx;
}

function isTransferred( Ctx, Game, For ) {
    if( Ctx.transferredtickets === undefined ) {
        return false;
    }

    if( Ctx.transferredtickets[ Game ] === undefined ) {
        return false;
    }

    if( Ctx.transferredtickets[ Game ]['from'][ For ] === undefined ) {
        return false;
    }

    return true;
}

function isBanned( Ctx, Game, For ) {
    if( Ctx.bans === undefined ) {
        return false;
    }

    if( Ctx.bans[ Game ] === undefined ) {
        return false;
    }

    if( Ctx.bans[ Game ].indexOf( For ) < 0 ) {
        return false;
    }

    return true;

}

// arglist is object
function Internal_issueSeasonTicket( IssueTo, ZeroEntitlement ) {
    var Result;
    var Ctx = initialize();

    var EntitlementTicket = {
        'maindomain' : 'ticket',
        'entitlementid' : TCD.generateUUID(),
        'name' : "Season ticket football",
        'domains' : [ 'ticket' ],
        'issuedtoid' : IssueTo,
        'issuerid' : 'football',
        'publicproperties' : {
            'type' : 'seasonticket',
            'id' : IssueTo,
            'zeroentitlement' : ZeroEntitlement
        }
    };

    Ctx.seasontickets[ IssueTo ] = EntitlementTicket;

    Result = TCD.send( "storemanager",
                      "addEntitlement",
                      { "entitlement" : EntitlementTicket }
                      );

    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    Context.set( Ctx );

    Result = { "result" : "Ticket purchased" };

    return Result;
}

// arglist is object
function issueSeasonTicket( ArgList ) {
    return Internal_issueSeasonTicket( ArgList['issuedtoid'], '' );
}

function issueSeasonTicketByEntitlemenet( ArgList ) {
    return Internal_issueSeasonTicket( ArgList['entitlement']['issuedtoid'], ArgList['entitlement'] );
}

// arglist is object
function transferTicket( ArgList ) {
    var Result;
    var Ctx = initialize();

    // request ticket from storemamanger
    Result = TCD.send( "storemanager",
                      "getEntitlementByDomain",
                      { "issuedtoid" :  ArgList['issuedtoid'],
                        "domain" : 'ticket'
                      }
                      );

    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    var Ents = Result.result.entitlements;
    var KnownTicket = Ctx.seasontickets[ ArgList['issuedtoid'] ];

    if( Ents === undefined ) {
        Result = { "error" : "No ticket for " + ArgList['issuedtoid'] + " found" };
        return Result;
    }

    if( KnownTicket === undefined ) {
        Result = { "error" : "No known ticket for " + ArgList['issuedtoid'] + " found" };
        return Result;
    }

    var TheTicket;
    for( var E in Ents ) {
        console.log( "Check " + E + KnownTicket.entitlementid );
        if( KnownTicket.entitlementid == E ) {
            // this ticket is known !!
            TheTicket = E;
            break;
        }
    }

    if( TheTicket === undefined ) {
        Result = { "error" : "Tickets for " + ArgList['issuedtoid'] + " found but none known" };
        return Result;
    }

    // check if person already has a ticket
    if( isTransferred( Ctx, ArgList['gameid'], ArgList['transfertoid'] ) ) {
        Result = { "error" : ArgList['transfertoid'] +
            " already has a ticket for game " + ArgList['gameid'] + " transferred by " +
            Ctx.transferredtickets[ ArgList['gameid'] ].to[ ArgList['transfertoid'] ] };
        return Result;
    }

    // transfer the ticket for a particular game
    var EntitlementTransferTicket = {
        'maindomain' : 'ticket',
        'entitlementid' : TCD.generateUUID(),
        'name' : "Football for game " + ArgList[ 'gameid' ],
        'domains' : [ 'ticket' ],
        'issuedtoid' : ArgList['transfertoid'],
        'issuerid' : 'football',
        'publicproperties' : {
            'type' : 'gameticket',
            'id' : ArgList['transfertoid'],
            'game' : ArgList[ 'gameid' ],
            'zeroentitlement' : '',
            'transferredby' : Ents[E]
        }
    };

    if( Ctx.transferredtickets === undefined ) {
        Ctx.transferredtickets = {};
    }

    if( Ctx.transferredtickets[ ArgList[ 'gameid' ] ]  === undefined ) {
        Ctx.transferredtickets[ ArgList[ 'gameid' ] ] = { "from" : {}, "to" : {} };
    }

    Ctx.transferredtickets[ ArgList[ 'gameid' ] ]["from"][ ArgList['issuedtoid'] ] = ArgList['transfertoid'];
    Ctx.transferredtickets[ ArgList[ 'gameid' ] ]["to"][ ArgList['transfertoid'] ] = ArgList['issuedtoid'];

    // send transferred ticket
    Result = TCD.send( "storemanager",
                      "addEntitlement",
                      { "entitlement" : EntitlementTransferTicket }
                      );

    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    Context.set( Ctx );

    Result = { "result" : "Ticket transferred to " + ArgList['transfertoid'] };

    return Result;
}

function revokeTicket( ArgList ) {

    var Result;
    var Ctx = initialize();

    // request ticket from storemamanger
    Result = TCD.send( "storemanager",
                      "getEntitlementByDomain",
                      { "issuedtoid" : ArgList['transferredtoid'],
                        "domain" : 'ticket'
                      }
                      );

    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    // find ticket for the game
    var Ents = Result.result.entitlements;

    if( Ents === undefined ) {
        Result = { "error" : "No ticket for " + ArgList['issuedtoid'] + " found" };
        return Result;
    }

    var TheTicket;
    for( var E in Ents ) {
        var AnEnt = Ents[E];
        console.log( E + " " + AnEnt.publicproperties.type + " " +
                    AnEnt.publicproperties.transferredby.issuedtoid + " " +
                    AnEnt.publicproperties.game );
        if( AnEnt.publicproperties.type == 'gameticket' &&
            AnEnt.publicproperties.transferredby.issuedtoid == ArgList['issuedtoid'] &&
            AnEnt.publicproperties.game == ArgList['gameid'] ) {
            TheTicket = AnEnt;
            break;
        }
    }

    if( TheTicket === undefined ) {
        Result = { "error" : ArgList['transferredtoid'] + " has no ticket for game " +
            ArgList['gameid'] + " transferred by "+ ArgList['issuedtoid'] };
        return Result;
    }

    var Args = { "issuedtoid" : ArgList['transferredtoid'],
                 "entitlementid" : TheTicket.entitlementid
    };

    // request ticket from storemamanger
    Result = TCD.send( "storemanager", "removeEntitlement", Args );

    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    delete Ctx.transferredtickets[ ArgList[ 'gameid' ] ]["from"][ ArgList['issuedtoid'] ];
    delete Ctx.transferredtickets[ ArgList[ 'gameid' ] ]["to"][ ArgList['transfertoid'] ];

    Context.set( Ctx );

    Result = { "result" : "Ticket for game " + ArgList[ 'gameid' ]  + " transferred to " +
        ArgList['transfertoid'] + "revoked" };

    return Result;
}

function listTransferredTickets( ArgList ) {
    var transferredBy = ArgList[ 'issuedtoid' ];
    var Result = [];
    var Ctx = initialize();

    if( Ctx.transferredtickets !== undefined ) {
        for( var gameid in Ctx.transferredtickets ) {
            if( Ctx.transferredtickets[ gameid ][ "from" ] !== undefined &&
                Ctx.transferredtickets[ gameid ][ "from" ][ transferredBy ] !== undefined ) {
                Result.push( { "game" : gameid,
                                "to" : Ctx.transferredtickets[ gameid ][ "from" ][ transferredBy ]
                            } );
            }
        }
    }

    Result = { "list" : Result };
    return Result;
}

function setBanFor( ArgList ) {
    var Result;
    var Ctx = initialize();

    if( ArgList['banned'] ) {
        if( isBanned( Ctx, ArgList['gameid'], ArgList['issuedtoid'] ) ) {
            Result = { "result" :
                "Already banned</br><b>" + ArgList['issuedtoid'] + "</b></br>for game</br><b>" + ArgList['gameid'] + "</b>" };

        } else {

            if( Ctx.bans[ ArgList['gameid'] ] === undefined ) {
                Ctx.bans[ ArgList['gameid'] ] = [];
            }

            Ctx.bans[ ArgList['gameid'] ].push( ArgList['issuedtoid'] );

            Result = { "result" : "ban for</br><b>" + ArgList['issuedtoid'] + "</b></br>for game</br><b>" + ArgList['gameid'] + "</b></br>set" };
        }
    } else {
        if( isBanned( Ctx, ArgList['gameid'], ArgList['issuedtoid'] ) ) {
            var idx = Ctx.bans[ArgList['gameid'] ].indexOf( ArgList['issuedtoid'] );
            if( idx >= 0 ) {
                Ctx.bans[ArgList['gameid'] ].splice( idx, 1 );
                Result = { "result" : "ban for</br><b>" + ArgList['issuedtoid'] + "</b></br>for game</br><b>" + ArgList['gameid'] + "</b></br>lifted" };
            } else {
                Result = { "result" : "No ban for</br><b>" + ArgList['issuedtoid'] + "</b></br>for game</br><b>" + ArgList['gameid'] + "</b>" };

            }
        } else {
            Result = { "result" : "No ban for</br><b>" + ArgList['issuedtoid'] + "</b></br>for game</br><b>" + ArgList['gameid'] +"</b>" };
        }
    }

    Context.set( Ctx );

    return Result;
}

function getGameList( ArgList ) {
    return { "games" : DataContext.gamelist };
}

function reset( ArgList ) {
    Result = TCD.send( "storemanager",
                      "resetStore",
                      { "storeid" :  ArgList['storeid'] }
                      );


    if( Result.error !== undefined ) {
        // do NOT commit the context
        return Result;
    }

    var Ctx = Context.get();

    Ctx.bans = { };
    // by userID
    Ctx.seasontickets = { };
    // by game, from to
    Ctx.transferredtickets = {};
    Ctx.accesslog = [];

    Context.set( Ctx );

    return { "result" : "reset" };
}

function validate_ticket( ArgList ) {
    var forgame = ArgList[ "extradata" ]["gameid" ];
    var gateid = ArgList[ "extradata" ]["gateid" ];
    var Ticket = ArgList[ "entitlement"];
    var Result;
    var Ctx = Context.get();

    if( Ticket.maindomain !== 'ticket' ) {
        Result = { "error" : "Invalid ticket" };
    } else {
        // correct game
        if( Ticket.publicproperties.type === 'seasonticket' ) {
            // check if game was not transferred
            if( ! isTransferred( Ctx, forgame, Ticket.issuedtoid ) ) {
                if( ! isBanned( Ctx, forgame, Ticket.issuedtoid ) ) {
                    // ticket was not transferred -> give access
                    Result = { "result" : "Entry allowed" };
                } else {
                    Result = { "error" : "Entry disallowed. Stadion ban applied" };
                }
            } else {
                // else ticket was transferred
                Result = { "error" : "Entry disallowed. Game transferred to " +
                                 Ctx.transferredtickets[ forgame ]['from'][ Ticket.issuedtoid ] };
            }
        } else {
            if( Ticket.publicproperties.type === 'gameticket' ) {

                if( Ticket.publicproperties.game !== forgame ) {
                    Result = { "error" : "Ticket not for game " + forgame + " but for game " + Ticket.publicproperties.game };
                } else {
                    if( ! isBanned( Ctx, forgame, Ticket.issuedtoid ) ) {
                        // ticket was not transferred -> give access
                        Result = { "result" : "Entry allowed" };
                    } else {
                        Result = { "error" : "Entry disallowed. Stadion ban applied" };
                    }
                }

            } else {
                Result = { "error" : "Invalid ticket type" };
            }
        }

    }

    if( Ctx.accesslog === undefined ) {
        Ctx.accesslog = [];
    }

    Ctx.accesslog.push(
                { "gateid" : gateid,
                    "date" : Date.now().toString(),
                    "result" : Result,
                    "id" : Ticket
                } );

    Context.set( Ctx );

    return Result;
}

