angular.module('TrustChainApp', ['ngAnimate', 'angular-loading-bar', 'ngWebSocket'])
    .config(function($sceDelegateProvider) {
        $sceDelegateProvider.resourceUrlWhitelist([
            '**'
        ]);
    })
    .factory('TrustSocket', function($websocket){
        var chatStream = $websocket(chatserverURL);
        var output = {
            chatStream : chatStream
        };


        return output;

        //return { chatStream : { onMessage : function(){}} };
    })
    .controller('TrustChainController', ['$scope', '$http', '$animate', 'TrustSocket', function ($scope, $http, $animate, TrustSocket) {
        var trustchain = this;

        trustchain.sites = [];
        //[{url: "https://www.nu.nl", name: "Nu"}, {url: "http://www.deredactie.be", name:"De Redactie"}];
        trustchain.url = "";
        trustchain.state = 'login';
        //home, login, site
        trustchain.storeid = "";

        trustchain.goto = function(f){
            if(f === undefined){
                trustchain.state = 'home';
            }else {
                trustchain.state = 'site';
                trustchain.url = f;
            }
        };

        trustchain.logout = function(){
            trustchain.state = 'login';
            trustchain.sites = [];
            trustchain.storeid = "";
        };

        TrustSocket.chatStream.onMessage(function(res){
            var d = JSON.parse(res.data);
            console.log(d.arguments);
            if(d.arguments.data.command === "kiosk"){
                if(trustchain.storeid === d.arguments.data.storeid){
                    trustchain.logout();
                }else{
                    trustchain.state = 'home';
                    trustchain.sites = d.arguments.data.message;
                    trustchain.storeid = d.arguments.data.storeid
                }
            }
        });

        TrustSocket.chatStream.onClose(function(message){
            alert("Lost connection to WebSocket");
            location.reload();
        });
        TrustSocket.chatStream.onError(function(message){
            alert("Errored connection to WebSocket");
            location.reload();
        });
    }]);

