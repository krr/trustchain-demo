angular.module('TrustChainApp', ['ngAnimate', 'angular-loading-bar', 'ngWebSocket'])
    .factory('TrustSocket', function($websocket){
        var dataStream = $websocket(footballURL);

        console.log(dataStream);
        var output = {
            callbacks : [],
            ignoreResult : function(){},
            logResult : function(ans){
                if(ans.result !== undefined){
                    console.log(ans.result);
                }else{
                    console.log(ans);
                }
            },
            doCall : function(functionName, arguments, onAnswer){
                this.callbacks.push(onAnswer);
                dataStream.send({function: functionName, arguments: arguments});
            }
        };

        dataStream.onMessage(function(message) {
            var cb = output.callbacks.shift();
            cb(JSON.parse(message.data));
        });
        dataStream.onClose(function(message){
            alert("Lost connection to WebSocket");
            location.reload();
        });
        dataStream.onError(function(message){
            alert("Errored connection to WebSocket");
            location.reload();
        });

        return output
    })
    .controller('TrustChainController', ['$scope', '$http', '$animate', 'TrustSocket', function ($scope, $http, $animate, TrustSocket) {
        var trustchain = this;
        trustchain.matches = [];

        trustchain.personid = "jan";
        trustchain.log = "";


        TrustSocket.doCall("getGameList",{}, function(r){
            trustchain.matches = r.result.games;
        });

        trustchain.submitAction = function(isBan){
            if($(".active").length == 0){
                alert("No Match Selected");
                return;
            }
            var matchId = $(".active")[0].innerHTML;
            var personId = trustchain.personid;
            TrustSocket.doCall("setBanFor",{banned: isBan, gameid: matchId,  issuedtoid: personId}, function(r){
                $("#tcLog")[0].innerHTML = r.result.result;
            });
        }


    }]);

