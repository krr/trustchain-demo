var ip = "192.168.188.53";
var masterPort = "8081";

var getService = function(serviceName){
    var urlRequest = "http://"+ip+":"+masterPort;
    var services = {};
    var port = 0;

    $.ajax({
        type: 'POST',
        url: urlRequest,
        data: JSON.stringify({"function": "listServices"}),
        async: false
    }).done(function (result) {
        console.log("Connection to master succeeded");
        services = JSON.parse(result).result.serverlist;
        port = services[serviceName].baseport + 2;
    }).fail(function(result){
        alert("Could not connect to master");
        location.reload();
    });

    return "ws://"+ip+":"+port
};


var storemanagerURL = getService("storemanager");
var chatserverURL = getService("irc");
var footballURL = getService("football");
