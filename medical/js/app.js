angular.module('TrustChainApp', ['ngAnimate', 'angular-loading-bar', 'ngWebSocket'])
    .factory('TrustSocket', function($websocket){
        var dataStream = $websocket(storemanagerURL);
        var chatStream = $websocket(chatserverURL);


        var output = {
            callbacks : [],
            ignoreResult : function(){},
            logResult : function(ans){
                if(ans.result !== undefined){
                    console.log(ans.result);
                }else{
                    console.log(ans);
                }
            },
            doCall : function(functionName, arguments, onAnswer){
                this.callbacks.push(onAnswer);
                dataStream.send({function: functionName, arguments: arguments});
            },
            chatStream : chatStream
        };

        dataStream.onMessage(function(message) {
            var cb = output.callbacks.shift();
            cb(JSON.parse(message.data));
        });

        dataStream.onClose(function(message){
            alert("Lost connection to WebSocket");
            location.reload();
        });
        dataStream.onError(function(message){
            alert("Errored connection to WebSocket");
            location.reload();
        });

        return output
    })
    .controller('TrustChainController', ['$scope', '$http', '$animate', 'TrustSocket', function ($scope, $http, $animate, TrustSocket) {
        var trustchain = this;

        this.model = {};
        var model = trustchain.model;

        var idpFile = "";

        this.createStore = function(storeName, cb){
          TrustSocket.doCall("listStores",{}, function(r) {
              if (r.result.stores.includes(storeName)) {
                  cb();
              } else {
                  TrustSocket.doCall("createStore", {storeid: storeName}, function (r) {
                      TrustSocket.logResult(r);
                      cb();
                  });
              }
          });
        };

        this.resetStores = function(){
            trustchain.createStore("medDemo-patient", function(){
                trustchain.createStore("medDemo-doctor", function(){
                    TrustSocket.doCall("resetStore",{storeid: "medDemo-patient"},TrustSocket.logResult);
                    TrustSocket.doCall("resetStore",{storeid: "medDemo-doctor"},TrustSocket.logResult);
                    TrustSocket.doCall("updateFCMToken", { fcmtoken: "ws://", issuedtoid: "medDemo-doctor" }, TrustSocket.logResult);
                    TrustSocket.doCall("updateFCMToken", { fcmtoken: "ws://", issuedtoid: "medDemo-patient" }, TrustSocket.logResult);
                })
            });
        };

        this.constructIDPFile= function(method){
            var out =  idpFile + "procedure main() { " + method +"() }";
            return out
        };

        this.init = function(){
            var r = {
                "code" : trustchain.constructIDPFile("init") ,
                "inference" : 2
            };
            $http({
                method: 'POST',
                url: "http://adams.cs.kuleuven.be/idp/eval",
                data: JSON.stringify(r)
            }).then(function(succes){
                var result = succes.data;
                if(result.stdout !== ""){
                    result = JSON.parse(result.stdout);
                    Object.assign(model, result);
                }else{
                    console.log(result.stderr)
                }
            })
        };

        this.issue = function(id, from, to){
            TrustSocket.doCall("issueEntitlement",
                { "entitlement": {
                    "domains": [],
                    "entitlementid": id,
                    "issuedtoid": to,
                    "issuerid": from,
                    "maindomain": "",
                    "name": "",
                    "privateproperties": {},
                    "publicproperties": {} } },
                TrustSocket.logResult);
        };
        this.revoke = function(id, from){
            TrustSocket.doCall("removeIssuedEntitlement",
                {"entitlementid": id,
                 "issuerid": from },
                TrustSocket.logResult);
        };

        this.onAction = {
           'AllowDossierAccess("General")' : function(){
               trustchain.issue("GeneralDossierAccess","medDemo-doctor", "medDemo-patient");
               return "done"
           },
            'AllowDossierAccess("Specialist")' : function(){
                trustchain.issue("SpecialistDossierAccess","medDemo-doctor", "medDemo-patient");
                return "done"
            },
            'RevokeDossierAccess("General")' : function(){
                trustchain.revoke("GeneralDossierAccess","medDemo-doctor", "medDemo-patient");
                return "done"
            },
            'RevokeDossierAccess("Specialist")' : function(){
                trustchain.revoke("SpecialistDossierAccess","medDemo-doctor", "medDemo-patient");
                return "done"
            }
        };

        this.entitlementToModel = {
            "GeneralDossierAccess" : function(v){trustchain.model.people[0].access = v},
            "SpecialistDossierAccess" : function(v){trustchain.model.people[1].access = v}
        };

        this.progress = function(action){
            var issues = trustchain.onAction[action.name];
            if(issues !== undefined){
                if(issues() === "done"){
                    return;
                }
            }
            trustchain.model.action = action.name;
            var r = {
                "code" : trustchain.constructIDPFile("continue") ,
                "inference" : 2,
                "eInput": JSON.stringify(trustchain.model)
            };

            $http({
                method: 'POST',
                url: "http://adams.cs.kuleuven.be/idp/eval",
                data: JSON.stringify(r)
            }).then(function(succes){
                var result = succes.data;
                if(result.stdout !== ""){
                    result = JSON.parse(result.stdout);
                    Object.assign(model, result);
                    console.log(model);
                }else{
                    console.log(result.stderr)
                }
            })
        };

        this.resetSim = function(){
            this.init();
            this.resetStores();
        };

        $http({
            url: 'medical.idp',
            method: 'GET'
        }).then(function (succes) {
            idpFile = succes.data;
            trustchain.resetSim()
        });

        TrustSocket.chatStream.onMessage(function(res){
            var d = JSON.parse(res.data);
            console.log(d);
            if(d.arguments.action !== "add" && d.arguments.action!== "revoke"){
                console.log("Unknown message: ", res)
            }
            var addition = (d.arguments.action === "add");
            var id = d.arguments.id;
            var func = trustchain.entitlementToModel[id];
            if(func !== undefined){
                func(addition);
                trustchain.progress({name : "Update"});
            }else{
                console.log("No function for entitlement ", id);
            }
        });

        TrustSocket.chatStream.onClose(function(message){
            alert("Lost connection to WebSocket");
            location.reload();
        });
        TrustSocket.chatStream.onError(function(message){
            alert("Errored connection to WebSocket");
            location.reload();
        });
    }]);

