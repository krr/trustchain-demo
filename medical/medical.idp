LTCvocabulary V {
    type Time
    Start: Time
    partial Next(Time) : Time

    type Doctor isa string
    type Treatment isa string

    hasGuardian(Time)

    dossierAccess(Time, Doctor)
    referralTo(Time, Doctor)

    prescription(Time, Treatment)
    insurancePermission(Time, Treatment)
    guardianPermits(Time, Treatment)
    examinationResult(Time, Treatment)

    type Action constructed from {
        Do(Treatment),
        AskInsurance(Treatment),
        AskGuardian(Treatment),

        Consultation(Doctor),
        AllowDossierAccess(Doctor),
        RevokeDossierAccess(Doctor),

        AssignGuardian,
        ReleaseFromGuardian,
        Update
    }

    cured(Time)
    allowed(Time, Action)

    doAction(Time):Action


    doctorRefers(Doctor, Doctor)

    guardianLikes(Treatment)
    generalAccess(Doctor)
}


theory T : V {

    {
        allowed(t,Do(tr)) <- insurancePermission(t,tr) & (hasGuardian(t) => guardianPermits(t,tr)) & prescription(t,tr).
        allowed(t,AskInsurance(tr)) <- prescription(t,tr) & ~insurancePermission(t,tr).
        allowed(t,AskGuardian(tr)) <- prescription(t,tr) & hasGuardian(t) & ~guardianPermits(t,tr).
        allowed(t,Consultation(d)) <- (referralTo(t,d) | generalAccess(d)) & dossierAccess(t,d).
        allowed(t,AllowDossierAccess(d)) <- ~dossierAccess(t,d).
        allowed(t,RevokeDossierAccess(d)) <- dossierAccess(t,d).
        allowed(t,AssignGuardian) <- ~hasGuardian(t).
        allowed(t,ReleaseFromGuardian) <- hasGuardian(t).
        allowed(t,Update).
    }

    ! t : allowed(t,doAction(t)).

    {
        dossierAccess(Next(t), d) <- dossierAccess(t,d) & ~(doAction(t) = RevokeDossierAccess(d)).
        dossierAccess(Next(t), d) <- doAction(t) = AllowDossierAccess(d).
    }
    {
        referralTo(Next(t), d) <- referralTo(t, d) & ~(doAction(t) = Consultation(d)).
        referralTo(Next(t), "Specialist") <- doAction(t) = Consultation("General") & ~cured(t) & ~(? x : prescription(t,x)).
        referralTo(Next(t), "Specialist") <- doAction(t) = Do("Scan").
    }
    {
        prescription(Next(t), tr) <- prescription(t, tr) & ~(doAction(t) = Do(tr)).
        prescription(Next(t), "Scan") <- doAction(t) = Consultation("Specialist") & ~examinationResult(t,"Scan").
        prescription(Next(t), "Operation") <- doAction(t) = Consultation("Specialist") & examinationResult(t,"Scan") & ~cured(t).
        prescription(Next(t), "Kinesitherapy") <- doAction(t) = Do("Operation").
        prescription(Next(t), "Kinesitherapy") <- doAction(t) = Do("Kinesitherapy").
    }
    {
        insurancePermission(Next(t), tr) <- insurancePermission(t, tr) & ~(doAction(t) = Do(tr)).
        insurancePermission(Next(t), tr) <- doAction(t) = AskInsurance(tr).
    }
    {
        guardianPermits(Next(t), tr) <- guardianPermits(t, tr) & ~(doAction(t) = Do(tr)).
        guardianPermits(Next(t), tr) <- doAction(t) = AskGuardian(tr) & guardianLikes(tr).
    }
    {
        examinationResult(Next(t), tr) <- examinationResult(t, tr).
        examinationResult(Next(t), tr) <- doAction(t) = Do(tr).
    }
    {
        cured(Next(t)) <- cured(t).
        cured(Next(t)) <- doAction(t) = Do("Operation").
    }
    {
        hasGuardian(Start).
        hasGuardian(Next(t)) <- hasGuardian(t) & ~doAction(t)=ReleaseFromGuardian.
        hasGuardian(Next(t)) <- doAction(t)=AssignGuardian.
    }
}

structure S : V {
    Time = {0..4}
    Doctor = {General; Specialist}
    Treatment = {Scan ; Operation ; Kinesitherapy}

    generalAccess = {General}
    guardianLikes = {Scan ; Operation ; Kinesitherapy}


    doctorRefers = {
        General , Specialist;
        Specialist, Specialist
    }
}

structure Base : V_ss {
  Doctor = {General; Specialist}
  Treatment = {Scan ; Operation ; Kinesitherapy}
  doctorRefers = { "General","Specialist"; "Specialist","Specialist" }
  generalAccess = { "General" }
  guardianLikes = {"Scan" ; "Operation" ; Kinesitherapy}
}


include <table_utils>

procedure init(){
    stdoptions.splitdefs = false
    json = require "idpd3/dkjson"
    local str = initialise(T,S)[1]
    print(json.encode(exportStruc(str)))
}

procedure continue(){
    stdoptions.splitdefs = false
    json = require "idpd3/dkjson"
    local li = io.read()
    local lio = json.decode(li)
    importStruc(lio)
    local new = progress(T,Base)[1]
    print(json.encode(exportStruc(new)))
}

procedure importStruc(obj){
    local dossierAccess = {}
    local referralTo = {}
    for i,d in pairs(obj.people) do
        if(d.access) then
           table.insert(dossierAccess, {d.name})
        end
        if(d.referral) then
           table.insert(referralTo, {d.name})
        end
    end
    Base[V_ss::dossierAccess].ct = dossierAccess
    Base[V_ss::dossierAccess].pt = dossierAccess
    Base[V_ss::referralTo].ct = referralTo
    Base[V_ss::referralTo].pt = referralTo


    local prescription = {}
    local insurance = {}
    local guardian = {}
    local results = {}
    for i,d in pairs(obj.treatments) do
        if(d.prescription) then
           table.insert(prescription, {d.name})
        end
        if(d.insurance) then
           table.insert(insurance, {d.name})
        end
        if(d.guardian) then
           table.insert(guardian, {d.name})
        end
        if(d.results) then
           table.insert(results, {d.name})
        end
    end
    Base[V_ss::prescription].ct = prescription
    Base[V_ss::prescription].pt = prescription
    Base[V_ss::insurancePermission].ct = insurance
    Base[V_ss::insurancePermission].pt = insurance
    Base[V_ss::guardianPermits].ct = guardian
    Base[V_ss::guardianPermits].pt = guardian
    Base[V_ss::examinationResult].ct = results
    Base[V_ss::examinationResult].pt = results

    local action = interpret(Base,obj.action)
    Base[V_ss::doAction].graph.ct = {{action}}
    Base[V_ss::doAction].graph.pt = {{action}}

    if(obj.hasGuardian) then
        Base[V_ss::hasGuardian].ct = {{}}
    else
        Base[V_ss::hasGuardian].cf = {{}}
    end

    if(obj.cured) then
            Base[V_ss::cured].ct = {{}}
    else
            Base[V_ss::cured].cf = {{}}
    end


    clean(Base)
}


//interpret a string as a domain element
procedure interpret(S,j){
    local typing = string.match(j,"\[^%[]*%[ : ([^%]]*)")
    local construct, args = string.match(j,"([^%(]+)%((.*)%)")
    if construct == nil then
        construct = string.match(j,"([^%[]+)")
    end
    local argList
    if args ==nil then
        argList = {}
    else
        args = args .. ","
        local sep = ","
        argList = {args:match((args:gsub("[^"..sep.."]*"..sep, "([^"..sep.."]*)"..sep)))}
    end

    for i,v in ipairs(argList) do
        argList[i] = interpret(S,v)
    end

    if(V[construct] ~= nil and typing ~= nil) then
        return S[V[construct][{V[typing].type}]](unpack(argList))
    elseif (V[construct] ~= nil) then
        return S[V[construct]](unpack(argList))
    elseif (tonumber(j)) then
        return tonumber(j)
    else
        return string.sub(j,2,-2)
    end
}

procedure exportStruc(struc){
    local out = {}

    local people = {}
    local doctors = totable(struc[V_ss::Doctor.type])
    for i,d in pairs(doctors) do
        people[i] = {}
        people[i].name = d
        people[i].access = struc[V_ss::dossierAccess].ct(d)
        people[i].referral = struc[V_ss::referralTo].ct(d)
        people[i].needReferral = struc[V_ss::generalAccess].cf(d)
    end
    out.people = people

    local treatments = {}
    local treats = totable(struc[V_ss::Treatment.type])
    for i,d in pairs(treats) do
        treatments[i] = {}
        treatments[i].name = d
        treatments[i].prescription = struc[V_ss::prescription].ct(d)
        treatments[i].insurance = struc[V_ss::insurancePermission].ct(d)
        treatments[i].guardian = struc[V_ss::guardianPermits].ct(d)
        treatments[i].results = struc[V_ss::examinationResult].ct(d)
    end
    out.treatments = treatments

    local actions = {}
    local acts = totable(struc[V_ss::allowed].ct)
    for i,d in pairs(acts) do
        actions[i] = {}
        actions[i].name = tostring(d[1])
    end
    out.actions = actions

    out.hasGuardian = struc[V_ss::hasGuardian].ct()
    out.cured = struc[V_ss::cured].ct()

    return out
}

