
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "");
};

angular.module('TrustChainApp', ['ngAnimate', 'angular-loading-bar', 'ngWebSocket'])
    .factory('TrustSocket', function($websocket){
        var dataStream = $websocket(storemanagerURL);
        var chatStream = $websocket(chatserverURL);

        console.log(dataStream);
        var output = {
            callbacks : [],
            ignoreResult : function(){},
            logResult : function(ans){
                if(ans.result !== undefined){
                    console.log(ans.result);
                }else{
                    console.log(ans);
                }
            },
            doCall : function(functionName, arguments, onAnswer){
                this.callbacks.push(onAnswer);
                dataStream.send({function: functionName, arguments: arguments});
            },
            chatStream : chatStream
        };

        dataStream.onMessage(function(message) {
            var cb = output.callbacks.shift();
            cb(JSON.parse(message.data));
        });

        dataStream.onClose(function(message){
            alert("Lost connection to WebSocket");
            location.reload();
        });
        dataStream.onError(function(message){
            alert("Errored connection to WebSocket");
            location.reload();
        });

        return output
    })
    .controller('TrustChainController', ['$scope', '$http', '$animate', 'TrustSocket', function ($scope, $http, $animate, TrustSocket) {
        var trustchain = this;
        trustchain.matches = [];

        trustchain.personid = "jan";
        trustchain.log = "";
        trustchain.sites = "";

        var logResultPublically = function(r){
            if(r.result !== undefined){
                logResultPublically(r.result);
            }else{
                $("#tcLog")[0].innerHTML += JSON.stringify(r)+"<br>";
            }
        };
        var resetLog = function(){
            $("#tcLog")[0].innerHTML = "";
        };

        trustchain.submitAction = function(){
            resetLog();

            var personid = trustchain.personid;

            var sitesObj = [];

            trustchain.sites.trim().split(/\n/).forEach(function(str){
                var url = str.substr(0,str.indexOf(' '));
                var nm = str.substr(str.indexOf(' ')+1);
                sitesObj.push({url: url, name: nm});
            });

            logResultPublically(sitesObj);


            TrustSocket.doCall("createStore",{storeid: personid}, logResultPublically);
            TrustSocket.doCall("addEntitlement",
                {"allowupdate":true,
                    "entitlement":{
                        "domains":[],
                        "entitlementid":"kioskEntitlement",
                        "issuedtoid":personid,
                        "issuerid":"kiosk",
                        "maindomain":"kiosk",
                        "name":"kioskEntitlement",
                        "privateproperties":{},
                        "publicproperties":{
                            "urls": sitesObj
                        }}}, logResultPublically);
        };

        trustchain.loadSubs = function() {
            resetLog();
            trustchain.sites = "";

            TrustSocket.doCall("getEntitlement",
                {entitlementid: "kioskEntitlement",
                issuedtoid: trustchain.personid},function(r){
                    if(r.result === undefined){
                        logResultPublically(r);
                        return;
                    }
                    var urls = r.result.entitlement.publicproperties.urls;
                    logResultPublically(urls);
                    urls.forEach(function(s){
                       trustchain.sites += s.url + " " + s.name + "\n";
                    });
                });
        };

        $scope.$watch('trustchain.personid', trustchain.loadSubs);

        trustchain.createKiosk = function() {
            TrustSocket.doCall("createStore",{storeid: "kiosk"}, logResultPublically);
            TrustSocket.doCall("updateFCMToken", { fcmtoken: "ws://", issuedtoid: "kiosk" }, logResultPublically);
        };

        TrustSocket.chatStream.onMessage(function(res){
            var d = JSON.parse(res.data);
            console.log(d.arguments);
            if(d.arguments.data.command === "updateKiosk"){
                console.log(d.arguments);
                trustchain.personid = d.arguments.data.storeid;
            }
        });

        TrustSocket.chatStream.onClose(function(message){
            alert("Lost connection to WebSocket");
            location.reload();
        });
        TrustSocket.chatStream.onError(function(message){
            alert("Errored connection to WebSocket");
            location.reload();
        });

        trustchain.createKiosk();


    }]);

